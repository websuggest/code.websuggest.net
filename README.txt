Go modules that are part of the WebSuggest organization should follow the
naming scheme: "code.websuggest.net/<module>". This is so we can freely
move between GitLab instances and code hosting providers with minimal
effort.

To add a new module to code.websuggest.net, add an entry to the 
"repositories" field of vangen.json with the name and repository URL
of the module, like this:

{
  "repositories": [
    ...
    {
      "prefix": "foobar",
      "url": "https://gitlab.com/websuggest/foobar"
    }
  ]
}


Documentation for vangen.json is available at:
https://github.com/leighmcculloch/vangen
